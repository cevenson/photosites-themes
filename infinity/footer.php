<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */
?>
		</section><!-- #main -->
		</div>
		<footer class="footer clearfix" role="contentinfo">

<?php
	/* A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	get_sidebar( 'footer' );
?>


			<a class="footer-name" href="<?php echo home_url( '/' ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?></a>

		</footer><!-- footer -->
		</div> <!-- container -->
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	wp_footer();
?>

<script type="text/javascript">
jQuery(document).ready(function($) {
    $("a.menu-toggle").click(function() {
        if ( $(".menu-main-menu-container").is(":hidden") ) {
            $(".menu-main-menu-container").slideDown("fast");
        } else {
            $(".menu-main-menu-container").slideUp("fast");
        }
    });
});
</script>
	</body>
</html>