<?php 
/**
Template Page for the gallery overview

Follow variables are useable :

	$gallery     : Contain all about the gallery
	$images      : Contain all images, path, title
	$pagination  : Contain the pagination content

 You can check the content when you insert the tag <?php var_dump($variable) ?>
 If you would like to show the timestamp of the image ,you can use <?php echo $exif['created_timestamp'] ?>
**/
?>


<!--
<script>

 jQuery( document ).ready( function( $ ) {          
 $('#masonrygallery').masonry({
    // options
    itemSelector : '.brick',
isAnimated: true,
  animationOptions: {
    duration: 500,
    easing: 'linear',
    queue: false}
  });  
  

  
  $('#masonrygallery .brick').hover(function() {
		
		$(this).css('cursor','pointer');
	}, function() {
		$(this).css('cursor','auto');
	});
      

  
  } );

</script>
-->




 

 
<?php if (!defined ('ABSPATH')) die ('No direct access allowed'); ?><?php if (!empty ($gallery)) : ?>



<div class="ngg-galleryoverview" id="<?php echo $gallery->anchor ?>">
<div id="masonry">
	<!-- Thumbnails -->
	<?php foreach ( $images as $image ) : ?>
	
	<div id="ngg-image-<?php echo $image->pid ?>"<?php echo $image->style ?> >
		<div class="brick" >
			<a href="<?php echo $image->imageURL ?>" title="<?php echo $image->description ?>" <?php echo $image->thumbcode ?> >
				<?php if ( !$image->hidden ) { ?>
				<img title="<?php echo $image->alttext ?>" alt="<?php echo $image->alttext ?>" src="<?php echo $image->thumbnailURL ?>" <?php echo $image->size ?> />
				<?php } ?>
			</a>
		</div>
	</div>
	
	<?php if ( $image->hidden ) continue; ?>
	

 	<?php endforeach; ?>



		<!-- Pagination -->
 	<?php echo $pagination ?>
</div>
</div>
	<?php endif; ?>