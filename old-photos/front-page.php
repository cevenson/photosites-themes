<?php
/**
 * Template Name: Home page
 *
 * 
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */

get_header(); ?>


<div class="row">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="grid_4">
<?php the_content(); ?>
<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'boilerplate' ), 'after' => '' ) ); ?>
				<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>
</div>

<div class="grid_8">				
				<!-- 	ACF Next gen custom field plugin code with galery type chooser -->	
	<?php
$gt = get_field('gallery_type'); 
if(get_field('gallery_chooser'))
{
	 foreach ( get_field ( 'gallery_chooser' ) as $nextgen_gallery_id ) :
        if ( $nextgen_gallery_id['ngg_form'] == 'album' ) {
            echo nggShowAlbum( $nextgen_gallery_id['ngg_id'] ); //NextGEN Gallery album
        } elseif ( $nextgen_gallery_id['ngg_form'] == 'gallery' ) {
             echo nggShowGallery( $nextgen_gallery_id['ngg_id'], $gt); //NextGEN Gallery gallery
        }
    endforeach;
}
 
?>

</div>


</div>
<div class="row">
<?php $posts = get_posts(array(
	'meta_query' => array(
		array(
			'key' => 'post_chooser',
			'value' => '1',
			'compare' => '=='
		)
	)
));
 
if( $posts )
{
	foreach( $posts as $post )
	{
		setup_postdata( $post );
 

global $more;
$more = 0;
?>
<div class="grid_4">
<article class="post-thumbs clearfix">
<?php if ( has_post_thumbnail()) : ?>
			<?php the_title( '<h2 class="entry-title"> <a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h2>' ); ?>
			
   <div class="ngg-gallery-thumbnail-box-iphone">
	<span class="tape">&nbsp;</span>
		<div class="ngg-gallery-thumbnail-iphone" >	
   <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
   <?php the_post_thumbnail('medium'); ?>
   </a>
		</div>
		</div>
   <?php else : ?>
   <?php the_title( '<h2 class="entry-title"> <a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h2>' ); ?>
   <?php endif; ?>
	
		<?php the_excerpt("...continue reading " . get_the_title('', '', false)); ?>

</article>
</div> <!-- end grid -->
<?php 
 
	}
 
	wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
}?>
</div> <!-- end row -->
<?php endwhile; ?>
	</div>

<?php get_footer(); ?>


