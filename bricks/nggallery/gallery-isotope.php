<?php 
/**
Template Page for the gallery overview

Follow variables are useable :

	$gallery     : Contain all about the gallery
	$images      : Contain all images, path, title
	$pagination  : Contain the pagination content

 You can check the content when you insert the tag <?php var_dump($variable) ?>
 If you would like to show the timestamp of the image ,you can use <?php echo $exif['created_timestamp'] ?>
**/
?>

 <script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.isotope.min.js" type="text/javascript"></script>


  <script>


 
    
 

    
    $(function(){
       var $container = $('#isotopegallery');
   


   $('#isotopegallery .photos').hover(function() {
		
		$(this).css('cursor','pointer');
	}, function() {
		$(this).css('cursor','auto');
	});




    // clicked element selects gallery
    $container.find('.photos').live('click', function(e){
	 var clickedElement = e.target;
			if($(this).hasClass('large')){
		       		$(this).toggleClass('large'); 
					$container.isotope({ 
			        layoutMode : 'masonry'
      			       });
					$container.isotope('reLayout');
			}else{		//make it big, if the enlarge button is clicked	
       				$(this).toggleClass('large'); 
					$container.isotope({ 
			       layoutMode : 'masonry'
      			       });
    	       $container.isotope('reLayout');
    	}; //end has large
    });



    $container.isotope({
          itemSelector : '.photos'
        });



      
    });
    
  </script>


 
 

 
<?php if (!defined ('ABSPATH')) die ('No direct access allowed'); ?><?php if (!empty ($gallery)) : ?>



	
	<!-- Thumbnails -->
		
	<div id="isotopegallery" class="clearfix">
		
		<?php foreach ( $images as $image ) : ?>
<div class="photos">

        <?php if ( !$image->hidden ) { ?>
        <img src="<?php echo $image->imageURL ?>" alt="<?php echo $image->alttext ?>" />
        <?php } ?>

</div>
 
    <?php endforeach; ?>

	</div>
	<?php endif; ?>