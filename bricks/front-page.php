<?php
/**
 * Template Name: Home page
 *
 * 
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */

get_header(); ?>


<div class="row">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>





				
					<!-- 	ACF Next gen custom field plugin code with galery type chooser -->	
	<?php
$gt = get_field('gallery_type'); 
if(get_field('gallery_chooser'))
{
	 foreach ( get_field ( 'gallery_chooser' ) as $nextgen_gallery_id ) :
        if ( $nextgen_gallery_id['ngg_form'] == 'album' ) {
            echo nggShowAlbum( $nextgen_gallery_id['ngg_id'] ); //NextGEN Gallery album
        } elseif ( $nextgen_gallery_id['ngg_form'] == 'gallery' ) {
             echo nggShowGallery( $nextgen_gallery_id['ngg_id'], $gt); //NextGEN Gallery gallery
        }
    endforeach;
}
 
?>
<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'boilerplate' ), 'after' => '' ) ); ?>
				<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>



<?php endwhile; ?>
	</div>

<?php get_footer(); ?>


