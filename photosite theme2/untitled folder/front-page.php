<?php
/**
 * Template Name: Home page
 *
 * 
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */

get_header(); ?>
<div class="row-fluid">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div id="fp-slider">
	

 <?php
    foreach ( get_field ( 'slider_chooser' ) as $nextgen_gallery_id ) :
        if ( $nextgen_gallery_id['ngg_form'] == 'album' ) {
            echo nggShowSlideshow($nextgen_gallery_id['ngg_id'],940,940);	//NextGEN Gallery album
        } elseif ( $nextgen_gallery_id['ngg_form'] == 'gallery' ) {
              echo nggShowSlideshow($nextgen_gallery_id['ngg_id'],940,940); //NextGEN Gallery gallery
        }
    endforeach;
?>
</div>


<div class="welcome"><?php the_field('welcome_message'); ?></div>
</div> <!-- row -->
<div class="row-fluid">
	<div class="span8">
				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'boilerplate' ), 'after' => '' ) ); ?>
				<?php edit_post_link( __( 'Edit', 'boilerplate' ), '', '' ); ?>



<?php endwhile; ?>
	</div>
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>

<script>
jQuery('.ngg-slideshow').width('100%');
jQuery('.ngg-slideshow').height('auto');
jQuery('.ngg-slideshow img').width('100% !important');
jQuery('.ngg-slideshow img').height('auto !important');
</script>