<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */
?>		
		</div> <!-- container -->
		</section><!-- #main -->
		<footer role="contentinfo">
			<div class="container">
<?php
	/* A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	get_sidebar( 'footer' );
?>
			</div>
					</footer><!-- footer -->
		
<!-- Load libraries and plugins -->
<script src='<?php bloginfo('template_directory'); ?>/js/vendor/bootstrap-min.js'></script>

<script src='<?php bloginfo('template_directory'); ?>/js/navigation-manager.js'></script>

<!-- Remove any navigation patterns below that arent used -->
<!-- // @javascript nav-toggle -->
<!-- <script src='<?php bloginfo('template_directory'); ?>/js/nav-patterns/left-nav-flyout.js'></script> -->
<script src='<?php bloginfo('template_directory'); ?>/js/nav-patterns/responsive-nav.min.js'></script>

<!-- Load configuration and initialise JavaScripts. 
	 Look in config.js to see how to turn items on/off -->
<script src='<?php bloginfo('template_directory'); ?>/js/config.js'></script>
<script src='<?php bloginfo('template_directory'); ?>/js/main.js'></script>
		
		
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */
	wp_footer();
?>
	</body>
</html>