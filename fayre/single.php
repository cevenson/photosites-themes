<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */

get_header(); ?>
<div class="row">
	<div class="grid_7">
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
<?php		if( get_field('show_featured_image') ) :?>
	<?php if ( has_post_thumbnail()) : ?>
	<div class="post-image-wrapper">
			<h1 class="entry-title"><?php the_title(); ?></h1>
   <?php the_post_thumbnail('large'); ?>
   </div> <!-- end post image wrapper -->
   <?php else : ?>
   <h1 class="entry-title no-post-image"><?php the_title(); ?></h1>
   <?php endif; ?>

<?php else : ?>
	<h1 class="entry-title no-post-image"><?php the_title(); ?></h1>

<?php endif; ?>

					
					<div class="entry-content clearfix">
	<!-- 	ACF Next gen custom field plugin code with galery type chooser -->	
	<?php
if(get_field('gallery_type'))
{
	$gt = get_field('gallery_type'); 
}
 

if(get_field('gallery_chooser'))
{
	 foreach ( get_field ( 'gallery_chooser' ) as $nextgen_gallery_id ) :
        if ( $nextgen_gallery_id['ngg_form'] == 'album' ) {
            echo nggShowAlbum( $nextgen_gallery_id['ngg_id'] ); //NextGEN Gallery album
        } elseif ( $nextgen_gallery_id['ngg_form'] == 'gallery' ) {
             echo nggShowGallery( $nextgen_gallery_id['ngg_id'], $gt); //NextGEN Gallery gallery
        }
    endforeach;
}
 
?>

						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '' . __( 'Pages:', 'boilerplate' ), 'after' => '' ) ); ?>
					</div><!-- .entry-content -->
<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>
					<footer id="entry-author-info">
						<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'boilerplate_author_bio_avatar_size', 60 ) ); ?>
						<h2><?php printf( esc_attr__( 'About %s', 'boilerplate' ), get_the_author() ); ?></h2>
						<?php the_author_meta( 'description' ); ?>
						<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
							<?php printf( __( 'View all posts by %s &rarr;', 'boilerplate' ), get_the_author() ); ?>
						</a>
					</footer><!-- #entry-author-info -->
<?php endif; ?>
					<footer class="entry-utility">
					<?php boilerplate_posted_on(); ?> |
						<?php boilerplate_posted_in(); ?>
						<?php edit_post_link( __( 'Edit', 'boilerplate' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-utility -->
				</article><!-- #post-## -->
				<nav id="nav-below" class="navigation clearfix">
					<div class="nav-previous"><?php previous_post_link( '<div class="icon-left-dir-1  btn">%link' . _x( '', 'boilerplate' ) . '</div> ' ); ?></div>
					<div class="nav-next"><?php next_post_link( '<div class="icon-right-dir-2  btn">%link' . _x( '', 'Next post link', 'boilerplate' ) . '</div>' ); ?></div>
				</nav><!-- #nav-below -->
				
				<?php comments_template( '', true ); ?>
<?php endwhile; // end of the loop. ?>
	</div>
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
